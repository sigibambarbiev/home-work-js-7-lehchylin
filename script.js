// 1. Опишіть своїми словами як працює метод forEach.

// Метод forEach запускает функцию для каждого элемента массива. Например, можно вывести в консоль все элементы массива и добавить к каждому символ $
// const emotions = ['happy', 'sad', 'angry'];
// emotions.forEach( emotion => console.log(emotion + '$') );
// В консоль будет выведено:
// 'happy$'
// 'sad$'
// 'angry$'

// 2. Як очистити масив?

// Для того чтобы очистить массив можно указать, что длина массива равна 0: arr.lenght=0, в этом случае все элемены массива будут удалены.
// delete.arr[] удаляет значение элемента массива, но пустой элемент остается и длина массива не изменится.

// 3. Як можна перевірити, що та чи інша змінна є масивом?

// Можно проверить с помощью специального метода Array.isArray()
// Если переменная это массив, то возвращает true, иначе - false.


const objectTest = {
    name: 'Valera',
    age: 32,
    country: 'Ukraine',
}

const arrayOriginal = [12, 5, 'Vasya', null, undefined, 'Pupkin', 345.4, objectTest];

function filterBy(arr, typeFilter) {
    return arr.filter(elem => typeof elem != typeFilter)
}

const arrayFilter = filterBy(arrayOriginal, 'number');

console.log(arrayFilter);


